from django.apps import AppConfig


class Story7AppConfig(AppConfig):
    name = 'story_7_app'
